package com.example.acesso.client.Cliente;

import com.example.acesso.exception.ClienteOfflineException;
import com.example.acesso.model.Cliente;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente getClientePorId(Long id) {
        throw new ClienteOfflineException();
    }
}