package com.example.acesso.client.Porta;

import com.example.acesso.exception.PortaNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 400) {
            throw new PortaNotFoundException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}