package com.example.acesso.client.Porta;

import com.example.acesso.exception.PortaOfflineException;
import com.example.acesso.model.Porta;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta getPortaPorId(Long id) {
        throw new PortaOfflineException();
    }
}