package com.example.acesso.controller;

import com.example.acesso.model.Acesso;
import com.example.acesso.model.dto.CreateAcessoRequest;
import com.example.acesso.model.dto.CreateAcessoResponse;
import com.example.acesso.model.dto.GetAcessoResponse;
import com.example.acesso.model.mapper.AcessoMapper;
import com.example.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper mapper;

    @PostMapping("/acesso")
    public CreateAcessoResponse postAcesso(@RequestBody CreateAcessoRequest createAcessoRequest){
        Acesso acesso = mapper.fromCreateRequest(createAcessoRequest);
        acesso = acessoService.create(acesso);
        return mapper.toCreateResponse(acesso);
    }

    @DeleteMapping("/acesso/{clienteId}/{portaId}")
    public void deleteAcesso(@PathVariable Long clienteId, @PathVariable Long portaId){
        acessoService.delete(clienteId, portaId);
    }

    @GetMapping("/acesso/{clienteId}/{portaId}")
    public GetAcessoResponse getAcesso(@PathVariable Long clienteId, @PathVariable Long portaId){
        Acesso acesso = acessoService.getById(clienteId, portaId);
        return mapper.toGetIdResponse(acesso);
    }

}