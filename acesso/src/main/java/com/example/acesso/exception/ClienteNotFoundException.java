package com.example.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "O Cliente informado não existe")
public class ClienteNotFoundException extends RuntimeException {
}