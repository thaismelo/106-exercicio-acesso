package com.example.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O microserviço de Porta está offline")
public class PortaOfflineException extends RuntimeException {
}