package com.example.acesso.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Acesso implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAcesso;
    private Long idPorta;
    private Long idCliente;

    public Long getIdAcesso() {
        return idAcesso;
    }

    public void setIdAcesso(Long idAcesso) {
        this.idAcesso = idAcesso;
    }

    public Long getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(Long idPorta) {
        this.idPorta = idPorta;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }
}