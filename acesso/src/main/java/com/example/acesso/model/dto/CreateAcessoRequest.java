package com.example.acesso.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAcessoRequest {

    @JsonProperty("porta_id")
    private Long idPorta;

    @JsonProperty("cliente_id")
    private Long idCliente;

    public Long getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(Long idPorta) {
        this.idPorta = idPorta;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }
}
