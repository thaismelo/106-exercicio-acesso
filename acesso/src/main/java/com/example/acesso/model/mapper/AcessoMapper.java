package com.example.acesso.model.mapper;

import com.example.acesso.model.Acesso;
import com.example.acesso.model.dto.CreateAcessoRequest;
import com.example.acesso.model.dto.CreateAcessoResponse;
import com.example.acesso.model.dto.GetAcessoResponse;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso fromCreateRequest(CreateAcessoRequest createAcessoRequest) {
        Acesso acesso = new Acesso();
        acesso.setIdCliente(createAcessoRequest.getIdCliente());
        acesso.setIdPorta(createAcessoRequest.getIdPorta());
        return acesso;
    }


    public static CreateAcessoResponse toCreateResponse(Acesso acesso) {
        CreateAcessoResponse createAcessoResponse = new CreateAcessoResponse();

        createAcessoResponse.setIdCliente(acesso.getIdCliente());
        createAcessoResponse.setIdPorta(acesso.getIdPorta());

        return createAcessoResponse;
    }

    public static GetAcessoResponse toGetIdResponse(Acesso acesso) {
        GetAcessoResponse getAcessoResponse = new GetAcessoResponse();

        getAcessoResponse.setIdCliente(acesso.getIdCliente());
        getAcessoResponse.setIdPorta(acesso.getIdPorta());

        return getAcessoResponse;
    }
}
