package com.example.acesso.repository;

import com.example.acesso.model.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Acesso findAcessoByIdClienteAndIdPorta(Long clienteId, Long portaId);

}
