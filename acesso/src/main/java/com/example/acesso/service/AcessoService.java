package com.example.acesso.service;

import com.example.acesso.client.Cliente.ClienteClient;
import com.example.acesso.client.Porta.PortaClient;
import com.example.acesso.exception.ClienteNotFoundException;
import com.example.acesso.exception.PortaNotFoundException;
import com.example.acesso.model.Acesso;
import com.example.acesso.model.Cliente;
import com.example.acesso.model.Porta;
import com.example.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;
    @Autowired
    private ClienteClient clienteClient;
    @Autowired
    private PortaClient portaClient;

    public Acesso create(Acesso acessoRequest){

        Cliente cliente = clienteClient.getClientePorId(acessoRequest.getIdCliente());
        if(cliente == null){
            throw new ClienteNotFoundException();
        }

        Porta porta = portaClient.getPortaPorId(acessoRequest.getIdPorta());
        if(porta == null){
            throw new PortaNotFoundException();
        }

        Acesso acesso = new Acesso();
        acesso.setIdCliente(cliente.getId());
        acesso.setIdPorta(porta.getId());

        return acessoRepository.save(acesso);
    }


    public void delete(Long clienteId, Long portaId){
        acessoRepository.delete(acessoRepository.findAcessoByIdClienteAndIdPorta(clienteId, portaId));
    }

    public Acesso getById(Long clienteId, Long portaId){
        Cliente cliente = clienteClient.getClientePorId(clienteId);
        if(cliente == null){
            throw new ClienteNotFoundException();
        }

        Porta porta = portaClient.getPortaPorId(portaId);
        if(porta == null){
            throw new PortaNotFoundException();
        }

        Acesso acesso = acessoRepository.findAcessoByIdClienteAndIdPorta(cliente.getId(), porta.getId());

        Acesso response = new Acesso();
        response.setIdCliente(acesso.getIdCliente());
        response.setIdPorta(acesso.getIdPorta());
        return response;
    }

}