package com.example.cliente.model.dto;

public class CreateClienteRequest {
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
