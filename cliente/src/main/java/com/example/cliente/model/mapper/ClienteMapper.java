package com.example.cliente.model.mapper;

import com.example.cliente.model.Cliente;
import com.example.cliente.model.dto.CreateClienteRequest;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {
    public Cliente toCliente(CreateClienteRequest createClienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setNome(createClienteRequest.getNome());
        return cliente;
    }
}
