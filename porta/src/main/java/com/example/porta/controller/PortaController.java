package com.example.porta.controller;

import com.example.porta.model.Porta;
import com.example.porta.model.dto.CreatePortaRequest;
import com.example.porta.model.mapper.PortaMapper;
import com.example.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class PortaController {
    @Autowired
    private PortaService portaService;

    @Autowired
    private PortaMapper mapper;

    @PostMapping("/porta")
    @ResponseStatus(HttpStatus.CREATED)
    public Porta create(@RequestBody CreatePortaRequest createPortaRequest) {
        Porta cliente = mapper.toPorta(createPortaRequest);
        cliente = portaService.create(cliente);

        return cliente;
    }

    @GetMapping("/porta/{id}")
    public Porta getById(@PathVariable String id){
        return portaService.getById(Long.valueOf(id));
    }
}
