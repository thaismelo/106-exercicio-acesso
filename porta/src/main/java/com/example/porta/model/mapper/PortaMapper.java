package com.example.porta.model.mapper;

import com.example.porta.model.Porta;
import com.example.porta.model.dto.CreatePortaRequest;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {
    public Porta toPorta(CreatePortaRequest createPortaRequest) {
        Porta porta = new Porta();
        porta.setAndar(createPortaRequest.getAndar());
        porta.setSala(createPortaRequest.getSala());
        return porta;
    }
}
