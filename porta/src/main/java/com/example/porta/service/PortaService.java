package com.example.porta.service;

import com.example.porta.exception.PortaNotFoundException;
import com.example.porta.model.Porta;
import com.example.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create(Porta porta) {
        return portaRepository.save(porta);
    }

    public Porta getById(Long id) {
        Optional<Porta> byId = portaRepository.findById(id);

        if(!byId.isPresent()) {
            throw new PortaNotFoundException();
        }

        return byId.get();
    }
}
